#!/usr/bin/env sh
# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================

set -e

function log_info() {
  echo -e "[\\e[1;94mINFO\\e[0m] $*"
}

function log_warn() {
  echo -e "[\\e[1;93mWARN\\e[0m] $*"
}

function log_error() {
  echo -e "[\\e[1;91mERROR\\e[0m] $*"
}

function fail() {
  log_error "$@"
  exit 1
}

function assert_defined() {
  if [[ -z "$1" ]]
  then
    fail "$2"
  fi
}

STAGES=".pre build test package-build package-test infra deploy acceptance publish infra-prod production .post"

# Determines whether the given project is a template project or not
# If so:
# - downloads the kicker.json file and adds some extra meta-data
# - downloads the README.md file
# $1: project JSON representation
# $2: extension ID in JSON (either `null` or `"some-id"``)
function maybe_process_template() {
  project_json="$1"
  extension_json="${2:-null}"
  project_id=$(echo "$project_json" | jq -r .id)
  project_name=$(echo "$project_json" | jq -r .path)
  project_path=$(echo "$project_json" | jq -r .path_with_namespace)
  project_default_branch=$(echo "$project_json" | jq -r .default_branch)

  # get tags (latest first, oldest last)
  all_tags=$(curl -sSf -H "$AUTH_HEADER" "$API_URL/projects/$project_id/repository/tags?per_page=100" | jq -r '.[].name' | sort -rV)
  latest_tag=$(echo "$all_tags" | head -n 1)
  tags_array_json=$(echo "$all_tags" | jq --raw-input . | jq --slurp --compact-output .)
  if [[ -n "$latest_tag" ]] && [[ "$latest_tag" != "null" ]]
  then
    project_path_enc=${project_path//\//%2f}
    web_url=$(echo "$project_json" | jq -r .web_url)
    add_info_json="{extension_id: $extension_json, project: {tag: \"$latest_tag\", tags: $tags_array_json, name: \"$project_name\", path: \"$project_path\", web_url: \"$web_url\", avatar: \"$web_url/-/avatar\"}}"

    # get kicker.json file from default branch
    if template_json=$(curl -sf -H "$AUTH_HEADER" "$API_URL/projects/$project_id/repository/files/kicker.json/raw?ref=$project_default_branch")
    then
      template_name=$(echo "$template_json" | jq -r ".name")
      template_desc=$(echo "$template_json" | jq -r ".description")
      template_kind=$(echo "$template_json" | jq -r ".kind")
      log_info "Project \\e[32m${project_path}\\e[0m declares template \\e[32m${template_name}\\e[0m of kind \\e[33;1m${template_kind}\\e[0m on latest tag \\e[33;1m${tag}\\e[0m"

      target_dir=${tmp_dir}/${project_path_enc}
      mkdir -p "$target_dir"

      # add additional info
      echo "$template_json" | jq ". + $add_info_json" > "$target_dir/template.json"

      # get README file from default branch
      log_info " ... downloading README.md to \\e[33;1m$DOC_OUT/ref/$project_name.md\\e[0m..."
      curl -sS -H "$AUTH_HEADER" --output "$DOC_OUT/ref/$project_name.md" "$API_URL/projects/$project_id/repository/files/README.md/raw?ref=$project_default_branch"

      # write template card
      {
        echo "-   <img class=\"tbc-tmpl-icon\" src=\"$web_url/-/avatar\"> [**$template_name**]($project_name)"
        echo
        echo "    ---"
        echo
        echo "    $template_desc"
        echo
        echo "    Version: \`$latest_tag\`"
        echo
      } >> "$DOC_OUT/ref/templates-grid.part.html"

      # extract stages
      template_path=$(echo "$template_json" | jq -r ".template_path")
      template_path_encoded=$(echo -n "$template_path" | jq -sRr @uri)
      mkdir -p $(dirname "$target_dir/$template_path")
      log_info " ... downloading $template_path as $template_path_encoded to \\e[33;1m$target_dir/$template_path\\e[0m..."
      curl -sS -H "$AUTH_HEADER" --output "$target_dir/$template_path" "$API_URL/projects/$project_id/repository/files/$template_path_encoded/raw?ref=$latest_tag"
      # write stages row
      {
        echo -n "<!-- $template_name --><tr>" # for alphabetical sort
        echo -n "<td class=\"tbc-tmpl-name\"><img class=\"tbc-tmpl-icon\" src=\"$web_url/-/avatar\"> <a href=\"$project_name\">$template_name</a></td>"
        for stage in $STAGES
        do
          if grep "^ *stage: $stage *$" "$target_dir/$template_path" >/dev/null 2>/dev/null
          then
            echo -n "<td>✔</td>"
          else
            echo -n "<td></td>"
          fi
        done
        echo "</tr>"
      } >> "$DOC_OUT/ref/templates-stages-body.part.html"

      # verify there is no new stage
      for stage in $(sed -n -e "s/^ *stage: \(.*\) *.*/\1/p" "$target_dir/$template_path" | sort -u)
      do
        if ! $(echo $STAGES | grep -w $stage >/dev/null 2>/dev/null)
        then
          log_warn "Stage '$stage' is unknown. Please update the internal STAGES constant."
        fi
      done

      # add entry to TOC
      case "$template_kind" in
        build|analyse|infrastructure|hosting|package|acceptance)
          toc_file="${tmp_dir}/toc/$template_kind"
          ;;
        *)
          toc_file="${tmp_dir}/toc/others"
          ;;
      esac
      echo "- $template_name: ref/$project_name.md" >> "$toc_file"
    else
      log_warn "Project \\e[32m${project_path}\\e[0m has no kicker.json file in latest tag \\e[33;1m${tag}\\e[0m: skip"
    fi

    # get kicker-extras.json file from default branch
    if extras_json=$(curl -sf -H "$AUTH_HEADER" "$API_URL/projects/$project_id/repository/files/kicker-extras.json/raw?ref=$project_default_branch")
    then
      project_path_enc=${project_path//\//%2f}
      web_url=$(echo "$project_json" | jq -r .web_url)
      add_info_json="{extension_id: $extension_json, project: {tag: \"$latest_tag\", tags: $tags_array_json, name: \"$project_name\", path: \"$project_path\", web_url: \"$web_url\"}}"

      log_info "Project \\e[32m${project_path}\\e[0m declares extra kicker resource(s) on latest tag \\e[33;1m${tag}\\e[0m..."

      # process (outer) variants
      for variant_b64 in $(echo "$extras_json" | jq -r '(.variants[])? | @base64')
      do
        variant_json=$(echo "$variant_b64" | base64 -d)
        variant_path=$(echo "$variant_json" | jq -r ".template_path")
        variant_name=$(echo "$variant_json" | jq -r ".name")
        target_project=$(echo "$variant_json" | jq -r ".target_project // empty")
        if [[ -n "$target_project" ]]
        then
          log_info " ... variant \\e[32m${variant_name}\\e[0m for \\e[33;1m${target_project}\\e[0m: file \\e[33;1m${variant_path}\\e[0m"

          target_project_enc=${target_project//\//%2f}
          target_dir=${tmp_dir}/${target_project_enc}/variants
          mkdir -p "$target_dir"
          target_file="$(echo $variant_name | tr '[:punct:]' '_')-$(date +%s).json"

          # add additional info & remove .for field
          echo "$variant_json" | jq "del(.for)" | jq ". + $add_info_json" > "$target_dir/$target_file"
        else
          log_warn " ... variant \\e[32m${variant_name}\\e[0m doesn't declare target template: ignore"
        fi
      done

      # process presets
      for preset_b64 in $(echo "$extras_json" | jq -r '(.presets[])? | @base64')
      do
        preset_json=$(echo "$preset_b64" | base64 -d)
        preset_name=$(echo "$preset_json" | jq -r ".name")
        log_info " ... preset \\e[32m${preset_name}\\e[0m"

        target_file="$(echo $preset_name | tr '[:punct:]' '_')-$(date +%s).json"

        # add additional info
        mkdir -p "$tmp_dir/presets"
        echo "$preset_json" | jq ". + $add_info_json" > "${tmp_dir}/presets/$target_file"
      done
    fi
  else
    log_info "Project \\e[32m${project_path}\\e[0m has no tag: skip"
  fi
  echo
}

# Iterates over all projects from given group with given visibility and tries to process it as a template project
# $1: GitLab group path
# $2: visibility to scan through
# $3: extension ID in JSON (either `null` or `"some-id"``)
function process_templates_from_group() {
  group_path="$1"
  group_id=${group_path//\//%2f}
  visibility="$2"
  extension_json="$3"
  exlude_projects="$4"
  log_info "--- Looking for projects with visibility \\e[33;1m${visibility}\\e[0m from group \\e[32m${group_path}\\e[0m..."

  for project_b64 in $(curl -sSf -H "$AUTH_HEADER" "$API_URL/groups/$group_id/projects?visibility=${visibility}&per_page=100&order_by=name&sort=asc" | jq -r '.[] | @base64')
  do
    project_json=$(echo "$project_b64" | base64 -d)
    project_path=$(echo "$project_json" | jq -r '.path')
    if echo "$exlude_projects" | grep -e "^${project_path}\$"
    then
      log_info "Project \\e[33;1m${project_path}\\e[0m matches excludes: skip"
    else
      maybe_process_template "$project_json" "$extension_json"
    fi
  done
}

function build_aggregated_json() {
  # 1: add variants to templates
  log_info "--- Adding outer variants to templates..."
  for tmpl_file in $(ls -1 ${tmp_dir}/*/template.json)
  do
    tmpl_dir=$(dirname "$tmpl_file")
    if [[ -d "$tmpl_dir/variants" ]]
    then
      log_info " ... adding outer variants to \\e[33;1m${tmpl_file}\\e[0m"
      variants_array_json=$(jq --slurp '.' $tmpl_dir/variants/*.json)
      jq ".variants+=$variants_array_json" < "$tmpl_file" > "$tmpl_dir/template-final.json"
    else
      cp "$tmpl_file" "$tmpl_dir/template-final.json"
    fi
  done

  # 2: grab all presets
  if [[ -d "$tmp_dir/presets" ]]
  then
  log_info "--- Assembling presets..."
    presets_array_json=$(jq --slurp '.' $tmp_dir/presets/*.json)
  else
    presets_array_json='[]'
  fi

  # 3: assemble
  jq --slurp '.' ${tmp_dir}/*/template-final.json | jq --compact-output "{extensions: $extensions_array_json, presets: $presets_array_json, templates: .}" > "$JSON_OUT"
}

function process_default_images() {
  # 1: build list of default TBC images
  {
    # base images
    jq -r '.templates[] |
      "\(.name)|main|\(.variables[]? | select(.name | test(".*_IMAGE$")) | "\(.name)|\(.default)")"' "$JSON_OUT"
    # features images
    jq -r '.templates[] |
        "\(.name)|feat|\(.features[]? | .variables[]? | select(.name | test(".*_IMAGE$")) | "\(.name)|\(.default)")"' "$JSON_OUT"
  } | awk '!/(\|null$|\$CI_REGISTRY_IMAGE)/{print}' | sort -f -t '|' -k3 > $IMAGES_OUT
  # exclude 'null' images or images containing '$CI_REGISTRY_IMAGE'

  # 2: build Trivy Reports TOC file
  toc_file="$tmp_dir/toc-trivy.md"
  while read -r line
  do
    var_name=$(echo "$line" | cut -d '|' -f3)
    touch "docs/secu/trivy-$var_name.md"
    echo "- $var_name: secu/trivy-$var_name.md"
  done < $IMAGES_OUT | uniq > "$toc_file"

  # 3: build Trivy Reports index
  while read -r line
  do
    tmpl_name=$(echo "$line" | cut -d '|' -f1)
    img_usage=$(echo "$line" | cut -d '|' -f2)
    var_name=$(echo "$line" | cut -d '|' -f3)
    img_uri=$(echo "$line" | cut -d '|' -f4)
    # create placeholder
    echo "<td>not fetched</td>" > docs/secu/trivy-${var_name}.part.html
    # write row in trivy-reports-body.part.html
    echo "<!-- $var_name --><tr class=\"img-$img_usage\"><td>$tmpl_name</td><td><a href=\"trivy-$var_name\">$var_name</a></td><td><code>$img_uri</code></td>"
    echo "--8<-- \"docs/secu/trivy-${var_name}.part.html\""
    echo "</tr>"
  done < $IMAGES_OUT > docs/secu/trivy-reports-body.part.html
}

function build_aggregated_toc() {
  log_info "--- Generating aggregated documentation TOC..."
  toc_file="$tmp_dir/toc-tmpl.md"
  {
    if [[ -f "$tmp_dir/toc/build" ]]; then
      echo "- Build & Test:"
      sort -f "$tmp_dir/toc/build" | sed 's/^/  /'
    fi
    if [[ -f "$tmp_dir/toc/analyse" ]]; then
      echo "- Code Analysis:"
      sort -f "$tmp_dir/toc/analyse" | sed 's/^/  /'
    fi
    if [[ -f "$tmp_dir/toc/package" ]]; then
      echo "- Packaging:"
      sort -f "$tmp_dir/toc/package" | sed 's/^/  /'
    fi
    if [[ -f "$tmp_dir/toc/infrastructure" ]]; then
      echo "- Infrastructure:"
      sort -f "$tmp_dir/toc/infrastructure" | sed 's/^/  /'
    fi
    if [[ -f "$tmp_dir/toc/hosting" ]]; then
      echo "- Deploy & Run:"
      sort -f "$tmp_dir/toc/hosting" | sed 's/^/  /'
    fi
    if [[ -f "$tmp_dir/toc/acceptance" ]]; then
      echo "- Acceptance:"
      sort -f "$tmp_dir/toc/acceptance" | sed 's/^/  /'
    fi
    if [[ -f "$tmp_dir/toc/others" ]]; then
      echo "- Others:"
      sort -f "$tmp_dir/toc/others" | sed 's/^/  /'
    fi
  } > "$toc_file"
}

# API url default to local GitLab instance
API_URL=${BASE_API_URL:-https://gitlab.com/api/v4}
JSON_OUT=kicker-aggregated.json
IMAGES_OUT=tbc-default-images.out
DOC_OUT=./docs

KICKER_RESOURCE_GROUPS=${KICKER_RESOURCE_GROUPS:-'[{"path": "to-be-continuous", "visibility": "public"}]'}

log_info parse params

# parse arguments
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
    log_info "Usage: $0 "
    log_info "       [--api <GitLab API url>]"
    log_info "       [--token <GitLab API token>]"
    log_info "       [--json-out <output JSON file>]"
    log_info "       [--doc-out <README output dir>]"
    log_info "       [--groups <JSON encoded GitLab groups to crawl>]"
    log_info "       [--tag-pattern <preferred tag pattern (regex)>]"
    echo
    log_info "Groups to crawl shall be formatted as follows:"
    log_info "[
      {
        \"path\": \"to-be-continuous\",
        \"visibility\": \"public\"
      },
      {
        \"path\": \"some-path/to/another-group\",
        \"visibility\": \"internal\",
        \"exclude\": [\"project-2\", \"project-13\"],
        \"extension\": {
          \"id\": \"ext-123\",
          \"name\": \"extension 123\",
          \"description\": \"to be continuous extension for 123\"
        }
      },
      {
        \"path\": \"some-path/to/another-group\",
        \"visibility\": \"public\",
        \"extension\": {
          \"id\": \"ext-XYZ\",
          \"name\": \"extension XYZ\",
          \"description\": \"to be continuous extension for XYZ\"
        }
      }
    ]"
    exit 0
    ;;
    --api)
    API_URL="$2"
    shift # past argument
    shift # past value
    ;;
    --token)
    GITLAB_TOKEN="$2"
    shift # past argument
    shift # past value
    ;;
    --json-out)
    JSON_OUT="$2"
    shift # past argument
    shift # past value
    ;;
    --doc-out)
    DOC_OUT="$2"
    shift # past argument
    shift # past value
    ;;
    --groups)
    KICKER_RESOURCE_GROUPS="$2"
    shift # past argument
    shift # past value
    ;;
    *) # unknown option
    POSITIONAL="$POSITIONAL $1" # save it in an array for later
    shift # past argument
    ;;
esac
done

if [[ "$GITLAB_TOKEN" ]]; then AUTH_HEADER="PRIVATE-TOKEN: $GITLAB_TOKEN"; fi

assert_defined "$API_URL" 'Missing required env $API_URL'

log_info "Processing templates:"
log_info "- GitLab API url     (--api)       : \\e[33;1m${API_URL}\\e[0m"
log_info "- kicker output file (--json-out)  : \\e[33;1m${JSON_OUT}\\e[0m"
log_info "- readme output dir  (--doc-out)   : \\e[33;1m${DOC_OUT}\\e[0m"
log_info "- groups to process  (--groups)    : \\e[33;1m${KICKER_RESOURCE_GROUPS}\\e[0m"

# create a temporary directory to download individual kicker files
tmp_dir=$(mktemp -d)
mkdir -p "$tmp_dir/toc"
mkdir -p "$DOC_OUT/ref"

log_info " ... working in: \\e[33;1m${tmp_dir}\\e[0m"

# prepare reference table
{
  echo "<th>stages</th>"
  for stg in $STAGES
  do
    echo "<th class=\"tbc-stage\"><code>$stg</code></th>"
  done
} > "$DOC_OUT/ref/templates-stages-head.part.html"

echo "" > "$DOC_OUT/ref/templates-grid.part.html"
echo "" > "$DOC_OUT/ref/templates-stages-body.part.html"

# iterate on groups
extensions_array_json='[]'
for group_b64 in $(echo "$KICKER_RESOURCE_GROUPS" | jq -r '.[] | @base64')
do
  group_json=$(echo "$group_b64" | base64 -d)

  # process templates from group
  group_path=$(echo "$group_json" | jq -r .path)
  group_visibility=$(echo "$group_json" | jq -r .visibility)
  exclude=$(echo "$group_json" | jq -r ".exclude[]? // empty")
  group_ext_json=$(echo "$group_json" | jq .extension.id)
  process_templates_from_group "$group_path" "$group_visibility" "$group_ext_json" "$exclude"

  # append extension if any
  group_extension=$(echo "$group_json" | jq .extension)
  if [[ "$group_extension" != "null" ]]
  then
    extensions_array_json=$(echo "$extensions_array_json" | jq ". += [$group_extension]")
  fi
done

# sort ref table
sort -f -o "$DOC_OUT/ref/templates-stages-body.part.html" "$DOC_OUT/ref/templates-stages-body.part.html"

# aggregate all kicker files into one + add extensions info
build_aggregated_json
log_info "Aggregated kicker file created: \\e[33;1m${JSON_OUT}\\e[0m"

# generate list of default TBC images (for security scan)
process_default_images
log_info "TBC default images file created: \\e[33;1m${IMAGES_OUT}\\e[0m"

build_aggregated_toc
if [[ -f "mkdocs.yml" ]]
then
  log_info "Injecting templates list in mkdocs menu..."
  toc=$(cat "$tmp_dir/toc-tmpl.md" | sed 's/^/    /')
  awk -v toc="${toc//&/\\\\&}" '{sub(/.*INSERT_TOC_HERE.*/,toc)}1' mkdocs.yml > mkdocs-tmp.yml
  cp -f mkdocs-tmp.yml mkdocs.yml

  log_info "Injecting Trivy reports in mkdocs menu..."
  toc=$(cat "$tmp_dir/toc-trivy.md" | sed 's/^/    /')
  awk -v toc="${toc//&/\\\\&}" '{sub(/.*INSERT_TRIVY_HERE.*/,toc)}1' mkdocs.yml > mkdocs-tmp.yml
  cp -f mkdocs-tmp.yml mkdocs.yml
fi
