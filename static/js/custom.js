(function($) {

  // Navigation scrolls
  $('.navbar-nav li a').bind('click', function(event) {
    $('.navbar-nav li').removeClass('active');
    $(this).closest('li').addClass('active');
  });

  $(".navbar-collapse a").on('click', function() {
    $(".navbar-collapse.collapse").removeClass('in');
  });

  // Video controls
  $("video").on('click', function() {
    if (this.paused) {
      this.play();
    } else {
      this.pause();
    }
  });

})(jQuery);
