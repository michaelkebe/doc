# To Be Continuous website

This project builds and exposes _to be continuous_ website as [GitLab pages](https://to-be-continuous.gitlab.io/doc/).

The documentation is built with [MkDocs](https://www.mkdocs.org/) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

It also generates the assembled Kicker descriptor (one single JSON file containing all Kicker resources).

## Build and test locally

1. Install [MkDocs](https://www.mkdocs.org/#installation) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/),
2. Run with `mkdocs server`,
3. Open [http://127.0.0.1:8000](http://127.0.0.1:8000) in your browser.

In this mode, MkDocs updates any change on-the-fly.

## How to configure the assembled Kicker descriptor build ?

The assembled Kicker descriptor is built by crawling one or several GitLab groups/projects, looking for Kicker descriptors.

It can be configured with the following variables:

* `GITLAB_TOKEN`: a [group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) with at least scopes `api,read_repository`
  and `Developer` role,
* `KICKER_RESOURCE_GROUPS`: JSON configuration of GitLab groups to crawl.

### Details about `KICKER_RESOURCE_GROUPS`

Here is an example of `KICKER_RESOURCE_GROUPS` content:

```json
[
  {
    "path": "acme/cicd/all", 
    "visibility": "public"
  },
  {
    "path": "acme/cicd/ai-ml", 
    "visibility": "internal", 
    "exclude": ["project-2", "project-13"],
    "extension": 
      {
        "id": "ai-ml", 
        "name": "AI/ML", 
        "description": "ACME templates for AI/ML projects"
      }
  },
  {
    "path": "to-be-continuous", 
    "visibility": "public"
  }
]
```

Some explanations:

* `path` is a path to a [GitLab group](https://docs.gitlab.com/ee/user/group/)
  with [GitLab projects](https://docs.gitlab.com/ee/user/project/) containing Kicker resources.
* `visibility` is the group/projects visibility to crawl.
* `exclude` (optional) allows to exclude some project(s) from processing.
* `extension` (optional) allows to associate Kicker resources with a separate extension (actionable within Kicker).

By default, `KICKER_RESOURCE_GROUPS` is configured to crawl the `to-be-continuous` group only.

## Tracking script configuration

Another thing that can be configured is how you will track audience on the _to be continuous_ website.

All you have to do is to defined a `TRACKING_JS` variable with tracking JavaScript code ([Google Analytics](https://analytics.google.com/), [Matomo](https://matomo.org/) or else).

Example of `TRACKING_JS` value for Google Analytics:

```javascript
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-123456789-1', 'auto');
ga('send', 'pageview');
```
