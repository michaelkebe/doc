# Templates Reference

## Templates

Here is the list of available templates:

<div class="grid cards" markdown>

--8<-- "docs/ref/templates-grid.part.html"

</div>

## Used Stages

Here is the list of [generic stages](../understand.md#generic-pipeline-stages) used by each _to-be-continuous_ template:

<table>
<thead class="tbc-stages"><tr>
--8<-- "docs/ref/templates-stages-head.part.html"
</tr></thead>
<tbody class="tbc-stages">
--8<-- "docs/ref/templates-stages-body.part.html"
</tbody>
</table>