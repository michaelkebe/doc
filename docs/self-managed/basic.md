---
author: Pierre Smeyers
---

# self-managed _to be continuous_ (basic)

This page details how to install and use _to be continuous_ (**tbc**) in a self-managed GitLab.

## Copy tbc to your GitLab

The first thing to do is to copy the complete [tbc group](https://gitlab.com/to-be-continuous/) structure to your own GitLab.

Don't panic, we provide all required tools to initiate it for the first time, and also schedule regular updates to keep synchronized.

### Pre-requisites

The GitLab Synchronization Script has the following requirements:

* Bash interpreter <br/>_Trivial on Linux or MacOS, tested with [Git Bash](https://www.atlassian.com/git/tutorials/git-bash) on Windows (available in [Git for Windows](https://gitforwindows.org/))_
* [curl tool](https://curl.se/) installed and accessible as `curl` command from the Bash interpreter
* [jq tool](https://stedolan.github.io/jq/download/) installed and accessible as `jq` command from the Bash interpreter

### Run the GitLab Copy CLI

1. Create an empty `to-be-continuous` root group with `public` visibility.
2. In the `to-be-continuous` root group, generate a [group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) with scopes 
   `api,read_registry,write_registry,read_repository,write_repository` and with `Owner` role.
3. Store the token in the `GITLAB_TOKEN` environment variable (`export GITLAB_TOKEN="<the token>"`).
4. Install our [GitLab Copy CLI](https://gitlab.com/to-be-continuous/tools/gitlab-cp#usage) (requires Python 3.11 or higher):
    ```bash
    pip install gitlab-cp --index-url https://gitlab.com/api/v4/projects/to-be-continuous%2Ftools%2Fgitlab-cp/packages/pypi/simple --upgrade
    ```
5. Run the following command to **recursively copy the tbc group** to your own server:
    ```bash
    gitlab-cp \
        --src-api https://gitlab.com/api/v4 \
        --src-sync-path to-be-continuous \
        --dest-api https://your.gitlab.host/api/v4 \
        --dest-sync-path to-be-continuous \
        --exclude samples \
        --exclude custom \
        --dest-token "$GITLAB_TOKEN"
    ```

That should take a while, but hopefully at the end you'll have cloned the complete _to be continuous_ group and projects :tada:.

!!! WARNING "Installing _to be continuous_ in a custom root group"
    By default and preferably, _to be continuous_ shall be installed:

    * in the `to-be-continuous` root group on your GitLab server,
    * with **public** visibility.

    If one or both of these requirements can't be met (because you're not allowed to create a root group in your organization and/or 
    not allowed to create projects with public visibility), please 
    [read the advanced usage chapter](./advanced.md#installing-tbc-in-a-custom-group).

## Build the tracking image

!!! WARNING "Deprecated"
    _to be continuous_ used to need the [tracking](https://gitlab.com/to-be-continuous/tools/tracking) Docker image to be successfully built and available locally in your
    Docker registry because it is used as a [service container](https://docs.gitlab.com/ee/ci/services/) by all templates.

    This is no longer required as in its latest versions, TBC is - by default - pulling the image [from the gitlab.com public registry](https://gitlab.com/to-be-continuous/tools/tracking/container_registry).

    This can be overridden. For more info, please [read the advanced usage chapter](./advanced.md#setup-tracking).

## Sync. your local copy of tbc

From this point, you can start using _to be continuous_ templates from your GitLab.
But it is highly probable that you'll want to get automatically latest updates / new templates from the
original _to be continuous_ project.

For this, you only have to create a [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) in your local copy of the [tools/gitlab-sync](https://gitlab.com/to-be-continuous/tools/gitlab-sync) project:

1. declare the CI/CD project variable `GITLAB_TOKEN` with the previously created token (mark it as [masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable)),
2. create a scheduled pipeline (for instance every day at 2:00 am).

All other required variables will be automatically retrieved from [GitLab CI predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

!!! WARNING
    From this point, you might not make any commit in any local copy of _to be continuous_ projects
    because it will get overwritten every night.

    If you need to modify template code, you'll have 2 options depending on your case:

    * if it's a general enhancement/fix: make a contribution to the Open Source [to be continuous project](https://gitlab.com/to-be-continuous/) and get the change through the synchronization task,
    * if it's a change specific to your company: see [advanced usage](advanced.md)

By the way, you can manually trigger a pipeline in your [tools/gitlab-sync](https://gitlab.com/to-be-continuous/tools/gitlab-sync) project
anytime to synchronize your _to be continuous_ copy.

## Add your custom CA certificates

If your company is using non-Default Trusted Certificate Authorities to generate server certificates, it is highly probable that some _to be continuous_ template jobs will fail because such or such tool failed validating SSL certificates.

As explained in the [usage guide](../usage.md#certificate-authority-configuration), each project might
define a `CUSTOM_CA_CERTS` variable either as a group/project variable, or in its `.gitlab-ci.yml` file to declare the custom company Certificate Authorities.

But there is also a global way to fix this. Ask your GitLab administrator to declare `DEFAULT_CA_CERTS`
as an [instance-level CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#for-an-instance).
Similar to `CUSTOM_CA_CERTS`, `DEFAULT_CA_CERTS` shall contain one or several certificates in [PEM format](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail).

Every _to be continuous_ template job - prior to executing - determines whether a `$CUSTOM_CA_CERTS` or else `$DEFAULT_CA_CERTS` is defined and adds its content to the trusted CA certificates.
