# Security

## Risks

CI/CD is the automation of most (if not all) process in your software lifecycle going from building to running the application in production. With so many things happening, attackers have a wide range of capabilities in case of a compromise: 

- introduce dubious components (i.e. backdoor) in the application,
- leak (to the Internet) credentials granting access to internal services or infrastructures,
- access said services or infrastructures,
- impersonate an employee,
- leak intellectual property,
- ...

Every CI/CD job is executed inside a **container image** which contains various tools required by the job. 
When you are executing the job, you are trusting the people building the image and the tools.

In some cases, the image may have been corrupted by an attacker. This is what we call a [supply chain attack](https://en.wikipedia.org/wiki/Supply_chain_attack). There are famous cases like the [Codecov incident](https://blog.gitguardian.com/codecov-supply-chain-breach/) in which hackers managed to leak credentials from several services and cloud platforms.

## Mitigations

### Variables scoping

To prevent leakage, CI/CD variables has to be present only in jobs that need them. By example in the Maven template, only the `mvn-sonar` job actually needs the `SONAR_TOKEN` environment variable so others jobs (`mvn-build`, `mvn-release`...) should not access this secret.

You can find more info about how to protect your CI/CD variables in the [official GitLab documentation](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-security).

### Image selection

As much as we can, we try to select either **official images** (ex: Maven, Python), or at least images maintained by an **active community**. Each of those images can be freely overridden with the appropriate configuration variable to select _fixed versions_ ([more info here](../usage.md#docker-images-versions)) or any alternative that would suit you more. 

By default, _to be continuous_ templates mostly use the `latest` tag from upstream images since it is a maintenance-less default that works for nearly everyone. However `latest` images are prone to supply chain attacks and are also likely to introduce breaking changes.

_to be continuous_ is not responsible of any possible security issue from a default container image.

You should either :

- use a fixed version tag : using the `maven:3.9.1` instead of `maven:latest` make sure you stay on a specific version of tools 
    - prefer official image with the least tools (prefer minimal size, Alpine distribution or even distro-less if you can find one suited to your needs), the more the image has, heavier is the risk
- build and maintain your own image with a chosen version for each tools
    - use no or a minimal distribution
    - upgrade tools and important components regularly with safe updates
    - root-less image are best to prevent some container escalation vulnerabilities against your runner provider

!!! tip

    Whenever building your own image or using an upstream image, you can use [Renovate](https://docs.renovatebot.com/) to watch updates for your tools, test the new version and integrate them seamlessly.

## Vulnerability Reports (Trivy)

!!! warning

    When reviewing vulnerabilities from containers, you have to consider the following principes :

    - containers are usually very short-lived in a CI/CD environment
    - no direct user access is possible 
    - most job does not expose any exteral services (i.e. HTTP server) making attacks reliant on user interaction very hard if not impossible to exploit
    
    In short, risks are often low in **the CI/CD context** but carefully reviewing vulnerabilities are an essential step to secure your pipeline.

Here are vulnerability reports for each default image used by _to be continuous_ templates (generated every day):

<table>
<thead><tr>
<th>Template</th>
<th>Image Variable</th>
<th>Default Image</th>
<th>Vulnerabilities</th>
</tr></thead>
<tbody>
--8<-- "docs/secu/trivy-reports-body.part.html"
</tbody>
</table>