# {{- escapeXML ( index . 0 ).Target }}

!!! WARNING "Trivy Image Scan"

    * Image: `{{- escapeXML ( index . 0 ).Target }}`
    * Scan date: {{ now | date "2006-01-02" }}

{{- range . }}

## {{ escapeXML .Target }} ({{ .Type | toString | escapeXML }})

<table>
  {{- if (eq (len .Vulnerabilities) 0) }}
  <tr><th colspan="6" class="all-good">No Vulnerabilities found</th></tr>
  {{- else }}
  <tr class="sub-header">
    <th>Package</th>
    <th>Vulnerability ID</th>
    <th>Severity</th>
    <th>Installed Version</th>
    <th>Fixed Version</th>
    <th>Links</th>
  </tr>
    {{- range (slice .Vulnerabilities 0 (min (len .Vulnerabilities) 50)) }}
  <tr class="trivy vuln severity-{{ escapeXML .Vulnerability.Severity }}">
    <td class="pkg-name">{{ escapeXML .PkgName }}</td>
    <td><a href={{ escapeXML .PrimaryURL | printf "%q" }} title={{ escapeXML .Title | printf "%q" }}>{{ escapeXML .VulnerabilityID }}</a></td>
    <td class="severity">{{ escapeXML .Vulnerability.Severity }}</td>
    <td class="pkg-version">{{ escapeXML .InstalledVersion }}</td>
    <td>{{ if .FixedVersion }}{{ escapeXML .FixedVersion }}{{ else }}<i>no fix available</i>{{ end }}</td>
    <td class="links" data-more-links="off">
      {{- range .Vulnerability.References }}
      <a href={{ escapeXML . | printf "%q" }}>{{ escapeXML . }}</a>
      {{- end }}
    </td>
  </tr>
    {{- end }}
    {{- if (gt (len .Vulnerabilities) 50) }}
  <tr><th colspan="6" class="admonition-title">{{ sub (len .Vulnerabilities) 50 }} other vulnerabilities found...</th></tr>
    {{- end }}
  {{- end }}
  {{- if (eq (len .Misconfigurations ) 0) }}
  <tr><th colspan="6" class="all-good">No Misconfigurations found</th></tr>
  {{- else }}
  <tr class="sub-header">
    <th>Type</th>
    <th>Misconf ID</th>
    <th>Check</th>
    <th>Severity</th>
    <th>Message</th>
  </tr>
    {{- range .Misconfigurations }}
  <tr class="trivy misconf severity-{{ escapeXML .Severity }}">
    <td class="misconf-type">{{ escapeXML .Type }}</td>
    <td>{{ escapeXML .ID }}</td>
    <td class="misconf-check">{{ escapeXML .Title }}</td>
    <td class="severity">{{ escapeXML .Severity }}</td>
    <td class="link" data-more-links="off"  style="white-space:normal;"">
      {{ escapeXML .Message }}
      <br>
        <a href={{ escapeXML .PrimaryURL | printf "%q" }}>{{ escapeXML .PrimaryURL }}</a>
      </br>
    </td>
  </tr>
    {{- end }}
  {{- end }}
</table>
{{- end }}

<script>
  window.onload = function() {
    document.querySelectorAll('td.links').forEach(function(linkCell) {
      var links = [].concat.apply([], linkCell.querySelectorAll('a'));
      [].sort.apply(links, function(a, b) {
        return a.href > b.href ? 1 : -1;
      });
      links.forEach(function(link, idx) {
        if (links.length > 2 && 2 === idx) {
          var toggleLink = document.createElement('a');
          toggleLink.innerText = "more links (toggle)";
          toggleLink.href = "#toggleMore";
          toggleLink.setAttribute("class", "toggle-more-links");
          linkCell.appendChild(toggleLink);
        }
        linkCell.appendChild(link);
      });
    });
    document.querySelectorAll('a.toggle-more-links').forEach(function(toggleLink) {
      toggleLink.onclick = function() {
        var expanded = toggleLink.parentElement.getAttribute("data-more-links");
        toggleLink.parentElement.setAttribute("data-more-links", "on" === expanded ? "off" : "on");
        return false;
      };
    });
  };
</script>
